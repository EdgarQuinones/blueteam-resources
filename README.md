# Blueteam Resources

## Contributing and Disclosing Resources

You may have developed resources for use in the NCAE Cyber Games competition and
wish to disclose these resources for compliance with the competition rules (or
just for notoriety).

Some examples of valid contributions are:

* Scripts/Binaries that do not specifically target the competition infrastructure
* CTF Walkthroughs for previous challenges that have been publicly released
  * See our [challenges repo](https://gitlab.com/c2-games/ctf/public-challenges)
    for public challenges from previous competitions
* Guidance for successful organization and competition preparation for beginners

Please [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
[this repository](https://gitlab.com/c2-games/blueteam-resources) and make a
[Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
You do not have to add any reviewers, but if you were asked to add resources by
a member of the team, you may add them as a reviewer.

**Note:** If you are not comfortable with the merge request process, you may
reach out to a member of our staff for other options of disclosure.

## Current Contributions

### California State Polytechnic University, Pomona

* Competition: 2023 NCAE CyberGames Finals (April 22nd)
  * Participant(s)/Author(s): Gerardo Solis
  * Resources:
    * https://github.com/Sol-Gerardo/CPP-NCAE-Cyber-Competition
    * https://github.com/Sol-Gerardo/CPP-NCAE-Cyber-Competition/tree/Bill's-notes
    * https://github.com/Sol-Gerardo/CPP-NCAE-Cyber-Competition/tree/Jerry's-Notes

### Moraine Valley Community College

* Competition: 2024 NCAE CyberGames Regionals (Midwest 2 - March 2nd)
  * Participant(s)/Author(s): Damien Santiago
  * Resources:
    * https://github.com/SYNT4X1/Bluekit

### New Jersey Institute of Technology

* Competition: 2024 NCAE CyberGames Regionals (Northeast 2 - March 9th)
  * Participant(s)/Author(s): Alfred Simpson, Michael Sluka, Daniel Marriello, Noah Jacobson
  * Resources:
    * https://github.com/NJITICC/NCAE_CyberGames_Prep

### Syracuse University

* Competition: 2023 NCAE CyberGames Northeast 2 (March 11th)
  * Participant(s)/Author(s): Jay Patel
  * Resources:
    * [inotify-script-watch.sh](resources/syracuse-university/inotify-script-watch.sh)

### Western Governor's University

* Competition: 2023 NCAE CyberGames Northwest 2 (March 25th)
  * Participant(s)/Author(s): Undefined
  * Resources:
    * https://github.com/WGU-CCDC/Blue-Team-Tools
    * https://github.com/Eratik-Automatik/NCAE

### Wilmington University

* Competition: 2023 NCAE CyberGames Northeast 2 (March 11th)
  * Participant(s)/Author(s): Undefined
  * Resources:
    * https://github.com/Cyberwildcats/NCAE-prep-repo

### University of South Florida

* Competition: 2024 NCAE CyberGames Regionals (South East - February 24th)
  * Participant(s)/Author(s): undefined
  * Resources:
    * https://github.com/gerbsec/cyberherd-scripts
* Competition: 2022 NCAE CyberGames
  * Participant(s)/Author(s): Undefined
  * Resources:
    * https://github.com/gengarth/NCAE-write-up

### University of Central Florida

* Competition: 2023 NCAE CyberGames Southeast 2 (April 1st)
  * Participant(s)/Author(s): Undefined
  * Resources:
    * https://github.com/orgs/HackUCF/repositories

### University of Florida

* Competition: 2024 NCAE CyberGames Regionals (Southeast 2 - March 9th)
  * Participants/Authors: Team15 - University of Florida
  * Resources:
    * https://github.com/ufsitblue/ufsitblue.github.io/blob/main/ncae/ncae_vids_notes.md
    * https://github.com/ufsitblue/ncae24
* Competition: 2023 NCAE CyberGames Southeast 2 (April 1st)
  * Participant(s)/Author(s): Undefined
  * Resources:
    * https://github.com/ufsitblue/ufsitblue.github.io

### California State University, San Bernardino

* Competition: 2024 NCAE CyberGames Regionals (Southwest 2 - March 16th)
  * Participant(s)/Author(s): Nate Cano, Yazan Temraz
  * Resources:
    * https://github.com/skitpack/NCAE-CSUSB-2024
